#include <iomanip>
#include <iostream>
#include <filesystem>

std::ostream& spaces(std::ostream& out, int count) {
    return out << std::setfill(' ') << std::setw(count) << "";
}

void directory_tree(const std::filesystem::path& path, int level = 0) {
    namespace fs = std::filesystem;

    try {
        for (const auto& p : fs::directory_iterator(path)) {
            spaces(std::cout, level) << p.path().filename() << '\n';
            if (p.is_directory() && !p.is_symlink())
                directory_tree(p.path(), level + 2);
        }
    }
    catch (std::exception& ex) {
        spaces(std::cout, level) << "-ERROR: " << ex.what() << '\n';
    }
}

int main(int argc, char* argv[]) {
    const char* path = (argc == 2 ? argv[1] : "/");

    std::cout << path << std::endl;
    directory_tree(path, 2);

    return 0;
}
